﻿using UnityEngine;
using GoogleMobileAds.Api;

public class AdmobTest : MonoBehaviour
{
	public AdPosition adPosition = AdPosition.Top;
	public string adUnitIdAndroid = "";
	public string adUnitIdIos = "";
	public string adUnitIdUnity = "";

	void Start ()
	{
		RequestBanner ();
	}

	void RequestBanner ()
	{
		#if UNITY_ANDROID
		string adUnitId = adUnitIdAndroid;
		#elif UNITY_IPHONE
		string adUnitId = adUnitIdIos;
		#else
		string adUnitId = adUnitIdUnity;
		#endif

		// Create a 320x50 banner at the top of the screen.
		BannerView bannerView = new BannerView (adUnitId, AdSize.Banner, adPosition);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder ().Build ();
		// Load the banner with the request.
		bannerView.LoadAd (request);
	}
}